output "service_bus_namespace" {
   description    = "Service bus namespace"
   value          = module.servicebus_deployment.namespace_id
}
output "key_vault" {
   description    = "Key vault containing the service bus connection strings"
   value          = module.servicebus_deployment.key_vault_uri
}
output "deployment_queues" {
   description    = "SDL deployment queues"
   value          = local.is_production ? local.deployer_queues_prod : local.deployer_queues_nonprod
}
output "domain_queues" {
   description    = "Domain specific queues"
   value          = local.is_production ? local.domain_queues_prod : local.domain_queues_nonprod
}
output "tags" {
   description    = "Azure resource tags"
   value          = local.tags
}