terraform {
    required_version = ">=0.12.7"
    # backend "azurerm" {
    #     environment             = "public"
    #     resource_group_name     = "s00388nrgp1terraform"
    #     storage_account_name    = "s00388nsta1terraform"
    #     container_name          = "terraform"
    #     key                     = "digitalcontent-deployment.terraform.tfstate"
    #     subscription_id         = "8870e2a8-4802-46e5-b359-487db75050ae"
    #     tenant_id               = "ee69be27-d938-4eb5-8711-c5e69ca43718"
    # }
}
provider "azuread" {
    version                     = "=0.6.0"
    environment                 = "public"
}
provider "azurerm" {
    version                     = "=1.33.0"
    environment                 = "public"
}
module "naming" {
    source                      = "./../../modules/starbucks-naming-convention"
    environment                 = var.environment
    location                    = var.location
    fgid                        = var.fgid
}

locals {
    is_production               = var.environment == "prod"
    tags = {
        appname                 = "Martech Digital Content"
        appdescription          = "Digital content services, including SDL and MediaBeacon SaaS integrations."
        costcenter              = var.cost_center
        dataclassification      = "level2"
        environment             = var.environment
        fgid                    = var.fgid
        location                = var.location
        objectname              = "(not set)"
        ppm                     = var.ppm
        support                 = var.support_contact
    }
    deployer_queues_prod        = [ 
        "digitalcontent-deployment-prod" 
    ]
    deployer_queues_nonprod    = [
        "digitalcontent-deployment-dev",
        "digitalcontent-deployment-stable"
    ]
    domain_queues_prod          = [
        "digitalcontent-deployment-akamai-prod",
        "digitalcontent-deployment-apigee-prod",
        "digitalcontent-deployment-folio-prod",
        "digitalcontent-deployment-partner-prod"
    ]
    domain_queues_nonprod      = [
        "digitalcontent-deployment-akamai-dev",
        "digitalcontent-deployment-akamai-stable",
        "digitalcontent-deployment-apigee-dev",
        "digitalcontent-deployment-apigee-stable",
        "digitalcontent-deployment-folio-dev",
        "digitalcontent-deployment-folio-stable",
        "digitalcontent-deployment-partner-dev",
        "digitalcontent-deployment-partner-stable"
    ]
    keyvault_non_queue_names_dev      = [
        "digitalcontent-deployment-dev-partner-elasticUrl",
        "ccu-access-token",
        "ccu-client-secret",
        "ccu-client-token",
        "ccu-host"
    ]
}

resource "azurerm_resource_group" "content" {
    name                        = format("%s%s", module.naming.resource_group_basename, "cms")
    location                    = var.location
    tags                        = merge(
        local.tags, {
            objectname          = format("%s%s", module.naming.resource_group_basename, "cms")
        })
}

module "servicebus_deployment" {
    source                      = "./../../modules/azure-servicebus"
    name                        = format("%s%s", module.naming.servicebus_namespace_basename, "cms-deployment")
    resource_group_name         = azurerm_resource_group.content.name
    tags                        = local.tags
    queues                      = {
        deployment              = local.is_production ? local.deployer_queues_prod : local.deployer_queues_nonprod
        notification_domains    = local.is_production ? local.domain_queues_prod : local.domain_queues_nonprod
    }
    admin_users                 = var.admins
}

module "functionapp_deployment" {
    source                      = "./../../modules/functionapp"
    resource_group_name         = azurerm_resource_group.content.name
    location                    = var.location
    tags                        = local.tags
    admin_users                 = var.admins
    key_vault_id                = module.servicebus_deployment.key_vault_id
    keyvault_non_queue_names_dev= local.keyvault_non_queue_names_dev
    keyvault_non_queue_values_dev= var.keyvault_non_queue_values_dev
}
