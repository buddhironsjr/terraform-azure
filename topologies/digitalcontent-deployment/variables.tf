variable "cost_center" {
   type        = string
   description = "Cost center tag"
}
variable "environment" {
   type        = string
   description = "Starbucks environment: prod, non-prod or sandbox"
}
variable "fgid" {
   type        = string
   description = "Functional group ID"
}
variable "location" {
   type        = string
   description = "Azure region name: eastus or westus"
}
variable "ppm" {
   type        = string
   description = "PPM project code"
   default     = ""
}
variable "support_contact" {
   type        = string
   description = "Resource support e-mail address"
   default     = "dl-sdl-cmsissues@starbucks.com"
}
variable "admins" {
   type        = list(string)
   description = "List of administrative users"  
}
variable "keyvault_non_queue_values_dev" {
   type        = list(string)
   description = "Values of non Queue secrets in Key Vault in sequence"  
}
