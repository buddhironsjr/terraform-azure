terraform {
   required_version = ">=0.12.7"
   # backend "azurerm" {
   #    environment             = "public"
   #    resource_group_name     = "s00388nrgp1terraform"
   #    storage_account_name    = "s00388nsta1terraform"
   #    container_name          = "terraform"
   #    key                     = "digitalcontent-secrets.terraform.tfstate"
   #    subscription_id         = "8870e2a8-4802-46e5-b359-487db75050ae"
   #    tenant_id               = "ee69be27-d938-4eb5-8711-c5e69ca43718"
   # }
}
provider "azuread" {
   version                     = "=0.6.0"
   environment                 = "public"
}
provider "azurerm" {
   version                     = "=1.33.0"
   environment                 = "public"
}
data "azurerm_client_config" "current" {}
data "azuread_users" "admins" {
   # user_principal_names       = var.admins
   object_ids                 = var.admins
}

module "naming" {
   source                      = "./../../modules/starbucks-naming-convention"
   environment                 = var.environment
   location                    = var.location
   fgid                        = var.fgid
}
locals {
   is_production               = var.environment == "prod"
   tags = {
      appname                 = "Martech DevOps"
      appdescription          = "Deployment automation for Martech."
      costcenter              = var.cost_center
      dataclassification      = "level2"
      environment             = var.environment
      fgid                    = var.fgid
      location                = var.location
      objectname              = "(not set)"
      ppm                     = var.ppm
      support                 = var.support_contact
   }
   admin_cert_permissions = [
      "backup", "create", "delete", "deleteissuers", "get", "getissuers", "import", "list", "listissuers", 
      "managecontacts", "manageissuers", "purge", "recover", "restore", "setissuers", "update"
   ]
   admin_key_permissions = [
      "backup", "create", "decrypt", "delete", "encrypt", "get", "import", "list", "purge", "recover", 
      "restore", "sign", "unwrapKey", "update", "verify", "wrapKey"
   ]
   admin_secret_permissions = [
      "backup", "delete", "get", "list", "purge", "recover", "restore", "set"
   ]
   admin_storage_permissions = [
      "backup", "delete", "deletesas", "get", "getsas", "list", "listsas", "purge", "recover", "regeneratekey", 
      "restore", "set", "setsas", "update"
   ]
}
resource "azurerm_resource_group" "devops" {
   name                        = format("%s%s", module.naming.resource_group_basename, "devops")
   location                    = var.location
   tags                        = merge(
      local.tags, {
         objectname            = format("%s%s", module.naming.resource_group_basename, "devops")
      }
   )
}
resource "azurerm_key_vault" "sdltridion" {
   resource_group_name              = azurerm_resource_group.devops.name
   location                         = local.tags.location
   name                             = format("%s%s", module.naming.key_vault_basename, "sdltridion")
   tenant_id                        = data.azurerm_client_config.current.tenant_id
   sku_name                         = "standard"
   enabled_for_deployment           = false
   enabled_for_disk_encryption      = false
   enabled_for_template_deployment  = false
   tags                             = merge(
      local.tags, {
         objectname                 = format("%s%s", module.naming.key_vault_basename, "sdltridion")
         description                = "Managed secrets for SDL Content-as-a-Service (SaaS)"
      })
}
resource "azurerm_key_vault_access_policy" "sdltridion-admins" {
   for_each                         = toset(data.azuread_users.admins.object_ids)
   key_vault_id                     = azurerm_key_vault.sdltridion.id
   tenant_id                        = data.azurerm_client_config.current.tenant_id
   object_id                        = each.key
   certificate_permissions          = local.admin_cert_permissions
   key_permissions                  = local.admin_key_permissions
   secret_permissions               = local.admin_secret_permissions
   storage_permissions              = local.admin_storage_permissions
}
resource "azurerm_key_vault_access_policy" "sdltridion-current" {
   /* required to let terraform manage keyvault secrets, if any */
   key_vault_id                     = azurerm_key_vault.sdltridion.id
   tenant_id                        = data.azurerm_client_config.current.tenant_id
   object_id                        = data.azurerm_client_config.current.client_id
   secret_permissions               = [ "list", "get", "set", "delete" ]
}

resource "azurerm_key_vault" "akamai" {
   resource_group_name              = azurerm_resource_group.devops.name
   location                         = local.tags.location
   name                             = format("%s%s", module.naming.key_vault_basename, "akamai")
   tenant_id                        = data.azurerm_client_config.current.tenant_id
   sku_name                         = "standard"
   enabled_for_deployment           = false
   enabled_for_disk_encryption      = false
   enabled_for_template_deployment  = false
   tags                             = merge(
      local.tags, {
         objectname                 = format("%s%s", module.naming.key_vault_basename, "akamai")
         description                = "Managed secrets for Akamai"
      })
}
resource "azurerm_key_vault_access_policy" "akamai-admins" {
   for_each                         = toset(data.azuread_users.admins.object_ids)
   key_vault_id                     = azurerm_key_vault.akamai.id
   tenant_id                        = data.azurerm_client_config.current.tenant_id
   object_id                        = each.key
   certificate_permissions          = local.admin_cert_permissions
   key_permissions                  = local.admin_key_permissions
   secret_permissions               = local.admin_secret_permissions
   storage_permissions              = local.admin_storage_permissions
}
resource "azurerm_key_vault_access_policy" "akamai-current" {
   /* required to let terraform manage keyvault secrets, if any */
   key_vault_id                     = azurerm_key_vault.akamai.id
   tenant_id                        = data.azurerm_client_config.current.tenant_id
   object_id                        = data.azurerm_client_config.current.client_id
   secret_permissions               = [ "list", "get", "set", "delete" ]
}
