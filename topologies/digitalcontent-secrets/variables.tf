variable "cost_center" {
   type        = string
   description = "Cost center tag"
}
variable "environment" {
   type        = string
   description = "Starbucks environment: prod, non-prod or sandbox"
}
variable "fgid" {
   type        = string
   description = "Functional group ID"
}
variable "location" {
   type        = string
   description = "Azure region name: eastus or westus"
   default     = "westus"
}
variable "location_code" {
   type        = number
   description = "Starbucks Azure region code: 0 = East US, 1 = West US"
   default     = 1
}
variable "ppm" {
   type        = string
   description = "PPM project code"
   default     = ""
}
variable "support_contact" {
   type        = string
   description = "Resource support e-mail address"
   default     = "dl-sre-martech@starbucks.com"
}
variable "admins" {
   type        = list(string)
   description = "List of administrative users"
}
