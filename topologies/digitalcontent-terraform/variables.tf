variable "cost_center" {
   type        = string
   description = "Cost center tag"
}
variable "environment" {
   type        = string
   description = "Starbucks environment: prod, non-prod or sandbox"
}
variable "fgid" {
   type        = string
   description = "Functional group ID"
}
variable "location" {
   type        = string
   description = "Azure region name: eastus or westus"
}
variable "admins" {
   type        = list(string)
   description = "User principal names of administrative users"
}
variable "support_contact" {
   type        = string
   description = "Resource support e-mail address"
   default     = "dl-sdl-cmsissues@starbucks.com"
}
