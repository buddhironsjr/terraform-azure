using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Starbucks.Martech.DigitalContent.Deployment.Shared;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Starbucks.Martech.DigitalContent.Deployment.ElasticSearch
{
    public static class ElasticSearchIndexerMain
    {
        [FunctionName("ElasticSearchIndexerMain")]
        public static async Task Run([ServiceBusTrigger("digitalcontent-deployment-partner-dev", Connection = "Keyvault_digitalcontent-deployment-partner-dev-listen-connstring")]string ElasticQueueItem, ILogger log)
        {
            log.LogInformation("Started indexing in ElasticSearch");

            JToken node = JToken.Parse(ElasticQueueItem);
            List<String> tcmIdList = new List<String>();

            string attrToSearch = EnvironmentVariables.SendToElastic;
            WalkNode(node, n =>
            {
                JToken token = n[attrToSearch];
                if (token != null && token.Type == JTokenType.String)
                {
                    tcmIdList.Add(token.Value<string>());
                }
            });

            string tcmIdListAsJson = JsonConvert.SerializeObject(tcmIdList).ToString();

            AzureKeyVaultService service = new AzureKeyVaultService();
            string elasticSearchUrl = await service.GetUrlFromKeyVault(EnvironmentVariables.PartnerElasticSearchIndexingUrl);
            HttpClient client = new HttpClient();

            var response = await client.PostAsync(elasticSearchUrl, new StringContent(tcmIdListAsJson, Encoding.UTF8, "application/json"));
            var responseString = await response.Content.ReadAsStringAsync();
            log.LogInformation($"Response from ElasticSearch URL: {responseString}");
        }


        static void WalkNode(JToken node, Action<JObject> action)
        {
            if (node.Type == JTokenType.Object)
            {
                action((JObject)node);

                foreach (JProperty child in node.Children<JProperty>())
                {
                    WalkNode(child.Value, action);
                }
            }
            else if (node.Type == JTokenType.Array)
            {
                foreach (JToken child in node.Children())
                {
                    WalkNode(child, action);
                }
            }
        }


    }
}

