﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Starbucks.Martech.DigitalContent.Deployment.Shared.Model
{
    class ItemWithMetadataAndKeywords : ItemWithMetadata
    {
        public List<Keyword> keywords { get; set; }
    }
}
