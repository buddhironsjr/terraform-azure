﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Starbucks.Martech.DigitalContent.Deployment.Shared.Model
{
    class Keyword : ItemWithMetadata
    {
        String key { get; set; }
        String taxonomy { get; set; }
        String totalRelatedItems { get; set; }
    }
}
