﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Starbucks.Martech.DigitalContent.Deployment.Shared.Model
{
    class ComponentPresentation
    {
        public Component component { get; set; }

        public Template template { get; set; }

    }
}
