﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Starbucks.Martech.DigitalContent.Deployment.Shared.Model
{
    class Binary : Item
    {
        public String variantId { get; set; }
        public String mimeType { get; set; }
    }
}
