using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Starbucks.Martech.DigitalContent.Deployment.Akamai
{
    class AkamaiPurgeObject
    {
        public List<String> objects; //= { "/" };

        public string hostname; //= "testsdlgreen.starbucks.com";

        public AkamaiPurgeObject(List<String> objects, string baseUrl)
        {
            this.hostname = baseUrl;
            this.objects = objects;

        }
    }
}
