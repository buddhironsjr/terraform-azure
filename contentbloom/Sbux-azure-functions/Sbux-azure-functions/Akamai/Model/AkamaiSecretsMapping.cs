using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Starbucks.Martech.DigitalContent.Deployment.Akamai
{
    public class AkamaiSecretsMapping
    {
        [JsonProperty("AccessToken")]
        public string AccessToken { get; set; }

        [JsonProperty("ClientSecret")]
        public string ClientSecret { get; set; }

        [JsonProperty("ClientToken")]
        public string ClientToken { get; set; }

        [JsonProperty("Host")]
        public string Host { get; set; }
    }
}
