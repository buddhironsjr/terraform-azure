environment     = "sandbox"
fgid            = "cbdev"
cost_center     = "00000"
location        = "southcentralus"
admins          = [
    "0e24598a-861c-4bca-a4a4-8cf134de8c1b", 
    "1f9652ce-9d73-4e44-b9d7-8d05c55d4efd"
]
keyvault_non_queue_values_dev =[
    "https://webhook.site/1b65ecc4-83d0-40ae-a029-e9c58b899905",
    "akab-lunfndv2yfegduhc-p5sjoxocyunlu5rh",
    "Xh4dB09HnhK6vAu+R0jU7Y9BmaTZKBq0o4kLRJ+Vhzo=",
    "akab-gxzxwzr64rew65by-erldvjr5em4bkfyd",
    "https://akab-zytwv2it2lplx7z3-ihdncyti2ncoqvy3.purge.akamaiapis.net"
]