# Configure the Microsoft Azure Provider
#  provide the values below
#  subscription_id : azure account id
#  client_id       : Service Principal username (appID)
#  client_secret   : Service Principal password
#  tenant_id       : Service Princiapl app tenant

provider "azurerm" {
  version = "~> 1.37"
  /* uncmmoent to use these for identity
  subscription_id = var.subscriptionID
  client_id       = var.servicePrincipalClientID
  client_secret   = var.servicePrincipalClientSecret
  tenant_id       = var.tenantID
  */
}

# Configure the Microsoft Azure Active Directory Provider,
#  provide the values below
#  subscription_id : var.subscriptionID
#  client_id       : Service Principal username (appID)
#  client_secret   : Service Principal password
#  tenant_id       : Service Princiapl app tenant
provider "azuread" {
  version = "~>0.3"
  /*
  subscription_id = var.subscriptionID
  client_id       = var.servicePrincipalClientID
  client_secret   = var.servicePrincipalClientSecret
  tenant_id       = var.tenantID
  */
}

# declare the application
# see https://www.terraform.io/docs/providers/azuread/r/application.html for defaults
# it will have an application_id after it is created-- this will be the client_id in the spec
resource "azuread_application" "ephemeralApplication" {
  name                       = "ephemeralApplication"
}

output "azure_active_directory_application_id" {
  value = azuread_application.ephemeralApplication.application_id
}

//for an existing resource, we have to configure it manually, start with just the name
//  then run terraform import azurerm_key_vault.sbux-akamai-api-creds2 <resource id>
//  then come back here and fill in the configuration, start by uncommenting the block below
resource "azurerm_key_vault" "YaddaKeyVault" {
  name                      = "YaddaKeyVault"
 
  location                  = var.location
  resource_group_name       = var.resourceGroupName
  tenant_id                 = var.tenantID

  //these are some default policy settings for teh service principal,
  //  need to verify that they match when the key vault is imported
  access_policy {
    tenant_id = var.tenantID
    object_id = var.servicePrincipalClientID

    key_permissions = [
       "get","list","update","create","import","delete","recover","backup","restore"
    ]
    #Secret permissions set for the service principal account (backup, get, list, recover, restore, set)
    secret_permissions = [
        "backup","get","list","recover","restore","set","delete"
    ]
    storage_permissions = [
        "get","list"
    ]
    certificate_permissions = [
      "get","list","update","create","import","delete","recover","backup","restore",
      "manageContacts",
      "manageIssuers","getIssuers","listIssuers","setIssuers","deleteIssuers"
    ]
  }  

  //here is where we configure the access policy for the app created above
  //  notice that we never make a note of the client ID, we just re-use it from the declaration above
  access_policy {
    tenant_id = var.tenantID
    object_id = azuread_application.ephemeralApplication.application_id

    key_permissions = [
      "get"
    ]

    secret_permissions = [
      "get"
    ]

    storage_permissions = [
      "get"
    ]
  }

}

# output "azure_existing_key_vault_application_id" {
#   value = azurerm_key_vault.YaddaKeyVault.id
# }
