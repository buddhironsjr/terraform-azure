variable "template" {
    type = "string"
    default = "01000000-0000-4000-8000-000030080200"
}
variable "users" {
    type    = "list"
    default = ["root", "user1", "user2"]
}
variable "plans" {
    type = "map"
    default = {
        "5USD"  = "1xCPU-1GB"
        "10USD" = "1xCPU-2GB"
        "20USD" = "2xCPU-4GB"
    }
}