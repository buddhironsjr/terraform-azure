environment     = "non-prod"
fgid            = "00388"
location        = "westus"
cost_center     = "802450"
admins          = [
   "m-00388-1@starbucks.com",
   "m-00388-2@starbucks.com",
   "m-00388-3@starbucks.com",
   "m-00388-4@starbucks.com",
   "m-00388-5@starbucks.com",
   "m-00388-6@starbucks.com",
   "m-00388-7@starbucks.com"
]