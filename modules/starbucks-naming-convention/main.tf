terraform {
   required_version = ">=0.12.7"
}

# =======================================================================
# Implements Starbucks naming convention for Azure 
# https://docs.starbucks.net/display/CS/Azure+Naming+and+Tagging+Standard
# =======================================================================

locals {
   prefix = join("", [
      "s",
      var.fgid,
      var.environment == "prod" ? "p" : var.environment == "non-prod" ? "n" : "s",
   ])
   location_code = var.location == "westus" ? "1" : "0"
}

locals {
   application_gateway_basename                 = format("%sapg%s", local.prefix, local.location_code)
   availability_set_basename                    = format("%saas%s", local.prefix, local.location_code)
   azure_ad_application_basename                = format("%sada%s", local.prefix, local.location_code)
   azure_adfs_basename                          = format("%sadf%s", local.prefix, local.location_code)
   azure_app_service_plan_basename              = format("%sapl%s", local.prefix, local.location_code)
   azure_automation_account_basename            = format("%saaa%s", local.prefix, local.location_code)
   azure_data_factory_basename                  = format("%sdfa%s", local.prefix, local.location_code)
   azure_data_lake_analytics_basename           = format("%sdla%s", local.prefix, local.location_code)
   azure_data_lake_store_basename               = format("%sdls%s", local.prefix, local.location_code)
   azure_domain_controller_basename             = format("%sadc%s", local.prefix, local.location_code)
   azure_gateway_basename                       = format("%sagw%s", local.prefix, local.location_code)
   azure_sql_data_warehouse_basename            = format("%ssdw%s", local.prefix, local.location_code)
   azure_sql_database_basename                  = format("%ssdb%s", local.prefix, local.location_code)
   azure_sql_server_basename                    = format("%sasq%s", local.prefix, local.location_code)
   cosmosdb_basename                            = format("%scdb%s", local.prefix, local.location_code)
   data_catalog_basename                        = format("%scat%s", local.prefix, local.location_code)
   dns_server_basename                          = format("%sdns%s", local.prefix, local.location_code)
   dns_zone_basename                            = format("%szon%s", local.prefix, local.location_code)
   documentdb_basename                          = format("%sddb%s", local.prefix, local.location_code)
   event_hub_basename                           = format("%sevh%s", local.prefix, local.location_code)
   express_route_circuit_basename               = format("%sexr%s", local.prefix, local.location_code)
   function_basename                            = format("%sfnc%s", local.prefix, local.location_code)
   gateway_public_ip_basename                   = format("%sgip%s", local.prefix, local.location_code)
   hd_insight_basename                          = format("%shdi%s", local.prefix, local.location_code)
   hybrid_runbook_worker_basename               = format("%shrw%s", local.prefix, local.location_code)
   iaas_sql_server_basename                     = format("%ssql%s", local.prefix, local.location_code)
   internal_load_balancer_basename              = format("%salb%s", local.prefix, local.location_code)
   iot_hub_basename                             = format("%siot%s", local.prefix, local.location_code)
   key_vault_basename                           = format("%sakv%s", local.prefix, local.location_code)
   load_balancer_public_ip_basename             = format("%svip%s", local.prefix, local.location_code)
   log_analytics_basename                       = format("%slog%s", local.prefix, local.location_code)
   logic_app_basename                           = format("%slga%s", local.prefix, local.location_code)
   machine_learning_workspace_basename          = format("%smlw%s", local.prefix, local.location_code)
   mongodb_basename                             = format("%smdb%s", local.prefix, local.location_code)
   network_adapter_basename                     = format("%snic%s", local.prefix, local.location_code)
   network_security_group_basename              = format("%snsg%s", local.prefix, local.location_code)
   network_security_group_definition_basename   = format("%snsg%s", local.prefix, local.location_code)
   network_security_group_rule_basename         = format("%sngr%s", local.prefix, local.location_code)
   notification_hubs_basename                   = format("%snot%s", local.prefix, local.location_code)
   public_ip_address_basename                   = format("%spip%s", local.prefix, local.location_code)
   recovery_service_vault_basename              = format("%srsv%s", local.prefix, local.location_code)
   redis_cache_basename                         = format("%srch%s", local.prefix, local.location_code)
   resource_group_basename                      = format("%srgp%s", local.prefix, local.location_code)
   route_table_basename                         = format("%sudr%s", local.prefix, local.location_code)
   route_table_route_basename                   = format("%srou%s", local.prefix, local.location_code)
   service_fabric_cluster_basename              = format("%ssfc%s", local.prefix, local.location_code)
   service_principal_name_basename              = format("%sspn%s", local.prefix, local.location_code)
   servicebus_namespace_basename                = format("%ssvb%s", local.prefix, local.location_code)
   splunk_server_basename                       = format("%sspl%s", local.prefix, local.location_code)
   storage_account_basename                     = format("%ssta%s", local.prefix, local.location_code)
   storage_blob_basename                        = format("%sstb%s", local.prefix, local.location_code)
   storage_table_basename                       = format("%sstt%s", local.prefix, local.location_code)
   stream_analytics_basename                    = format("%sstr%s", local.prefix, local.location_code)
   subnet_basename                              = format("%ssub%s", local.prefix, local.location_code)
   traffic_manager_profile_basename             = format("%stmp%s", local.prefix, local.location_code)
   virtual_network_basename                     = format("%svnt%s", local.prefix, local.location_code)
}