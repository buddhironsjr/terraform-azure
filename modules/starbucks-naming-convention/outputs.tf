output "application_gateway_basename" {
   description = "Base name for Azure Application Gateways"
   value       = local.application_gateway_basename
}
output "availability_set_basename" {
   description = "Base name for availability sets"
   value       = local.availability_set_basename
}
output "azure_ad_application_basename" {
   description = "Basename for Azure AD Applications"
   value       = local.azure_ad_application_basename
}
output "azure_adfs_basename" {
   description = "Basename for Azure AD Federation Service"
   value       = local.azure_adfs_basename
}
output "azure_app_service_plan_basename" {
   value       = local.azure_app_service_plan_basename
}
output "azure_automation_account_basename" {
   value       = local.azure_automation_account_basename
}
output "azure_data_factory_basename" {
   value       = local.azure_data_factory_basename
}
output "azure_data_lake_analytics_basename" {
   value       = local.azure_data_lake_analytics_basename
}
output "azure_data_lake_store_basename" {
   value       = local.azure_data_lake_store_basename
}
output "azure_domain_controller_basename" {
   value       = local.azure_domain_controller_basename
}
output "azure_gateway_basename" {
   value       = local.azure_gateway_basename
}
output "azure_sql_data_warehouse_basename" {
   value       = local.azure_sql_data_warehouse_basename
}
output "azure_sql_database_basename" {
   value       = local.azure_sql_database_basename
}
output "azure_sql_server_basename" {
   value       = local.azure_sql_server_basename
}
output "cosmosdb_basename" {
   value       = local.cosmosdb_basename
}
output "data_catalog_basename" {
   value       = local.data_catalog_basename
}
output "dns_server_basename" {
   value       = local.dns_server_basename
}
output "dns_zone_basename" {
   value       = local.dns_zone_basename
}
output "documentdb_basename" {
   value       = local.documentdb_basename
}
output "event_hub_basename" {
   value       = local.event_hub_basename
}
output "express_route_circuit_basename" {
   value       = local.express_route_circuit_basename
}
output "function_basename" {
   description = "Base name for Azure functions"
   value       = local.function_basename
}
output "gateway_public_ip_basename" {
   value       = local.gateway_public_ip_basename
}
output "hd_insight_basename" {
   value       = local.hd_insight_basename
}
output "hybrid_runbook_worker_basename" {
   value       = local.hybrid_runbook_worker_basename
}
output "iaas_sql_server_basename" {
   value       = local.iaas_sql_server_basename
}
output "internal_load_balancer_basename" {
   value       = local.internal_load_balancer_basename
}
output "iot_hub_basename" {
   value       = local.iot_hub_basename
}
output "key_vault_basename" {
   description = "Base name for key vaults"
   value       = local.key_vault_basename
}
output "load_balancer_public_ip_basename" {
   value       = local.load_balancer_public_ip_basename
}
output "log_analytics_basename" {
   value       = local.log_analytics_basename
}
output "logic_app_basename" {
   value       = local.logic_app_basename
}
output "machine_learning_workspace_basename" {
   value       = local.machine_learning_workspace_basename
}
output "mongodb_basename" {
   value       = local.mongodb_basename
}
output "network_adapter_basename" {
   value       = local.network_adapter_basename
}
output "network_security_group_basename" {
   value       = local.network_security_group_basename
}
output "network_security_group_definition_basename" {
   value       = local.network_security_group_definition_basename
}
output "network_security_group_rule_basename" {
   value       = local.network_security_group_rule_basename
}
output "notification_hubs_basename" {
   value       = local.notification_hubs_basename
}
output "public_ip_address_basename" {
   value       = local.public_ip_address_basename
}
output "recovery_service_vault_basename" {
   value       = local.recovery_service_vault_basename
}
output "redis_cache_basename" {
   value       = local.redis_cache_basename
}
output "resource_group_basename" {
   description = "Base name for resource groups"
   value       = local.resource_group_basename
}
output "route_table_basename" {
   value       = local.route_table_basename
}
output "route_table_route_basename" {
   value       = local.route_table_route_basename
}
output "service_fabric_cluster_basename" {
   value       = local.service_fabric_cluster_basename
}
output "service_principal_name_basename" {
   value       = local.service_principal_name_basename
}
output "servicebus_namespace_basename" {
   description = "Base name for service bus namespaces"
   value       = local.servicebus_namespace_basename
}
output "splunk_server_basename" {
   value       = local.splunk_server_basename
}
output "storage_account_basename" {
   value       = local.storage_account_basename
}
output "storage_blob_basename" {
   value       = local.storage_blob_basename
}
output "storage_table_basename" {
   value       = local.storage_table_basename
}
output "stream_analytics_basename" {
   value       = local.stream_analytics_basename
}
output "subnet_basename" {
   value       = local.subnet_basename
}
output "traffic_manager_profile_basename" {
   value       = local.traffic_manager_profile_basename
}
output "virtual_network_basename" {
   value       = local.virtual_network_basename
}