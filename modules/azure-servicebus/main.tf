terraform {
   required_version = ">=0.12.7"
}

data "azurerm_client_config" "current" {}
data "azuread_users" "admins" {
   # user_principal_names       = var.admins
   object_ids                 = var.admin_users
}

module "naming" {
   source               = "../starbucks-naming-convention"
   location             = var.tags.location
   environment          = var.tags.environment
   fgid                 = var.tags.fgid
}

resource "azurerm_key_vault" "deployment" {
   resource_group_name              = var.resource_group_name
   location                         = var.tags.location
   name                             = format("%s%s", module.naming.key_vault_basename, "cms-deploy")
   tenant_id                        = data.azurerm_client_config.current.tenant_id

   sku_name                         = "standard"
   enabled_for_deployment           = false
   enabled_for_disk_encryption      = false
   enabled_for_template_deployment  = false
   tags                             = merge(
      var.tags, {
         objectname                 = format("%s%s", module.naming.key_vault_basename, "cms-deploy")
      })
}

resource "azurerm_key_vault_access_policy" "admins" {
   for_each                         = toset(data.azuread_users.admins.object_ids)
   key_vault_id                     = azurerm_key_vault.deployment.id
   tenant_id                        = data.azurerm_client_config.current.tenant_id
   object_id                        = each.key
   secret_permissions               = [ "List", "Get", "Set", "Delete" ]
}
resource "azurerm_key_vault_access_policy" "current" {
   key_vault_id                     = azurerm_key_vault.deployment.id
   tenant_id                        = data.azurerm_client_config.current.tenant_id
   object_id                        = data.azurerm_client_config.current.client_id
   secret_permissions               = [ "List", "Get", "Set", "Delete" ]
}

resource "azurerm_servicebus_namespace" "instance" {
   name                 = var.name
   resource_group_name  = var.resource_group_name
   location             = var.tags.location
   sku                  = var.sizing.sku
   capacity             = var.sizing.capacity
   tags                 = merge(var.tags, {
      "objectname"      = var.name
   })
}

resource "azurerm_servicebus_queue" "deployment-queues" {
   for_each             = toset(var.queues.deployment)
   name                 = each.key
   resource_group_name  = azurerm_servicebus_namespace.instance.resource_group_name
   namespace_name       = azurerm_servicebus_namespace.instance.name

   dead_lettering_on_message_expiration    = true
   default_message_ttl                     = "PT15M"
   duplicate_detection_history_time_window = "PT5M"
   enable_express                          = false
   enable_partitioning                     = false
   lock_duration                           = "PT1M"
   max_delivery_count                      = 5
   max_size_in_megabytes                   = 1024
   requires_duplicate_detection            = false
   requires_session                        = false
}

resource "azurerm_servicebus_queue" "domain-queues" {
   for_each             = toset(var.queues.notification_domains)
   name                 = each.key
   resource_group_name  = azurerm_servicebus_namespace.instance.resource_group_name
   namespace_name       = azurerm_servicebus_namespace.instance.name

   dead_lettering_on_message_expiration    = true
   default_message_ttl                     = "PT5M"
   duplicate_detection_history_time_window = "PT1M"
   enable_express                          = false
   enable_partitioning                     = false
   lock_duration                           = "PT1M"
   max_delivery_count                      = 5
   max_size_in_megabytes                   = 1024
   requires_duplicate_detection            = false
   requires_session                        = false
}

resource "azurerm_servicebus_queue_authorization_rule" "deployer_send" {
   for_each             = toset(var.queues.deployment)
   resource_group_name  = var.resource_group_name
   namespace_name       = azurerm_servicebus_namespace.instance.name
   queue_name           = each.key
   name                 = format("%s-send", each.key)

   listen               = false
   send                 = true
   manage               = false
}
resource "azurerm_servicebus_queue_authorization_rule" "deployer_listen" {
   for_each             = toset(var.queues.deployment)
   resource_group_name  = var.resource_group_name
   namespace_name       = azurerm_servicebus_namespace.instance.name
   queue_name           = each.key
   name                 = format("%s-listen", each.key)

   listen               = true
   send                 = false
   manage               = false
}
resource "azurerm_servicebus_queue_authorization_rule" "domain_send" {
   for_each             = toset(var.queues.notification_domains)
   resource_group_name  = var.resource_group_name
   namespace_name       = azurerm_servicebus_namespace.instance.name
   queue_name           = each.key
   name                 = format("%s-send", each.key)

   listen               = false
   send                 = true
   manage               = false
}
resource "azurerm_servicebus_queue_authorization_rule" "domain_listen" {
   for_each             = toset(var.queues.notification_domains)
   resource_group_name  = var.resource_group_name
   namespace_name       = azurerm_servicebus_namespace.instance.name
   queue_name           = each.key
   name                 = format("%s-listen", each.key)

   listen               = true
   send                 = false
   manage               = false
}

resource "azurerm_key_vault_secret" "deployment_send" {
   depends_on = [
      azurerm_servicebus_queue_authorization_rule.deployer_send
   ]
   for_each          = toset(var.queues.deployment)
   key_vault_id      = azurerm_key_vault.deployment.id
   name              = format("%s%s", each.key, "-send-connection-string")
   content_type      = "text/plain"
   value             = format("%s;TransportType=AmqpWebSockets",
      azurerm_servicebus_queue_authorization_rule.deployer_send[each.value].primary_connection_string
   )
}
resource "azurerm_key_vault_secret" "deployment_listen" {
   depends_on = [
      azurerm_servicebus_queue_authorization_rule.deployer_listen
   ]
   for_each          = toset(var.queues.deployment)
   key_vault_id      = azurerm_key_vault.deployment.id
   name              = format("%s%s", each.key, "-listen-connection-string")
   content_type      = "text/plain"
   value             = azurerm_servicebus_queue_authorization_rule.deployer_listen[each.value].primary_connection_string
}
resource "azurerm_key_vault_secret" "domain_send" {
   depends_on = [
      azurerm_servicebus_queue_authorization_rule.domain_send
   ]
   for_each          = toset(var.queues.notification_domains)
   key_vault_id      = azurerm_key_vault.deployment.id
   name              = format("%s%s", each.key, "-send-connection-string")
   content_type      = "text/plain"
   value             = azurerm_servicebus_queue_authorization_rule.domain_send[each.value].primary_connection_string
}
resource "azurerm_key_vault_secret" "domain_listen" {
   depends_on = [
      azurerm_servicebus_queue_authorization_rule.domain_listen
   ]
   for_each          = toset(var.queues.notification_domains)
   key_vault_id      = azurerm_key_vault.deployment.id
   name              = format("%s%s", each.key, "-listen-connection-string")
   content_type      = "text/plain"
   value             = azurerm_servicebus_queue_authorization_rule.domain_listen[each.value].primary_connection_string
}