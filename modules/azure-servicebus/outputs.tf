output "namespace_id" {
   description    = "Service bus"
   value = azurerm_servicebus_namespace.instance.id
}
output "key_vault_uri" {
   description    = "Key vault containing service bus connection strings"
   value          = azurerm_key_vault.deployment.vault_uri
}
output "key_vault_id" {
   description    = "Key vault id for adding secrets from other modules "
   value          = azurerm_key_vault.deployment.id
}

