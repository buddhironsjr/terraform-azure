terraform {
   required_version = ">=0.12.7"
    backend "azurerm" {
    #     environment             = "public"
    #     resource_group_name     = "s00388nrgp1terraform"
    #     resource_group_name     = "scbdevsrgp0cms"
    #     storage_account_name    = "s00388nsta1terraform"
    #     container_name          = "terraform"
    #     key                     = "digitalcontent-deployment.terraform.tfstate"
    #     subscription_id         = "8870e2a8-4802-46e5-b359-487db75050ae"
    #     tenant_id               = "ee69be27-d938-4eb5-8711-c5e69ca43718"
    }
}

data "azurerm_client_config" "current" {}
data "azuread_users" "admins" {
   # user_principal_names       = var.admins
   object_ids                 = var.admin_users
}

module "naming" {
   source               = "../starbucks-naming-convention"
   location             = var.tags.location
   environment          = var.tags.environment
   fgid                 = var.tags.fgid
}

resource "azurerm_storage_account" "functionapp" {
  name                     = format("%s%s", module.naming.storage_account_basename , "terraform")
  resource_group_name      = var.resource_group_name
  location                 = var.tags.location
  account_tier             = var.tier
  account_replication_type = "LRS"
}

resource "azurerm_app_service_plan" "functionapp" {
  name                = format("%s%s", module.naming.azure_app_service_plan_basename  , "terraform")
  location            = var.tags.location
  resource_group_name = var.resource_group_name
  kind                = "FunctionApp"

  sku {
    tier = "Dynamic"
    size = "Y1"
  }
}

resource "azurerm_function_app" "functionapp" {
  name                      = format("%s%s", module.naming.function_basename  , "terraform")
  location                  = var.tags.location
  resource_group_name       = var.resource_group_name
  app_service_plan_id       = azurerm_app_service_plan.functionapp.id
  storage_connection_string = azurerm_storage_account.functionapp.primary_connection_string
}

resource "azurerm_key_vault_secret" "deployment_send" {
  count              = length(var.keyvault_non_queue_names_dev)
  content_type      = "text/plain"
   key_vault_id      = var.key_vault_id
   name              = var.keyvault_non_queue_names_dev[count.index]
   value             = var.keyvault_non_queue_values_dev[count.index]
}