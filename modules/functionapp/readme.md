# Deploy function app
+please add user object id in contentbloom/sandbox-cb.tfvars

+I see the reason with the error "resource not found" that occur some time 
Its because when we try to create secrets under KeyVault, for example, the KeyVault itself needs some time to be created so the script to create the secrets fails to find the KeyVault. In this scenario "depends_on[...]" might solve the issue

+the storage account name is created according to naming convesion. But it has to be globally unique. So we need to be sure to delete the existing storage in portal each time we run the script

List of changes made on December 06, 2019
1. digitalcontent-deployment/main.tf
-> +list: keyvault_non_queue_names_dev
-> +few modification in module: functionapp_deployment

2. digitalcontent-deployment/variables.tf
-> +var: keyvault_non_queue_values_dev

3. contentbloom/sandbox-cs.tfvars
-> +list: keyvault_non_queue_values_dev

4. MODULES/AZURE-SERVICEBUS/OUTPUTS.tf
-> +var: key_vault_id

5. modules/Functionapp/main.tf & variables.tf
-> adjestments